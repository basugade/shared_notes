--

select bdpln.* from bpm_def_step_actions bdsa
inner join bpm_def_steps bds on bdsa.step_id = bds.id
inner join bpm_def_stages bdst on bds.stage_id = bdst.id
inner join bpm_def_process_versions bdpv on bdpv.id = bdst.process_version_id
inner join bpm_def_processes bdp on bdp.id = bdpv.process_id
inner join bpm_def_processes_v bdpln on bdpln.id = bdp.id and bdpln.language_code = 'en'
where bdsa.script_id = 5093552;

--
select * from cmn_custom_scripts where script_text like '%Sending Email Failed for resource%';

