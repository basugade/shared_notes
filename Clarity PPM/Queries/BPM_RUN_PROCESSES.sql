select 
caption.name process_name,
defn.process_code,
runs.id process_instance_id,
runs.status_code,
runs.start_date,
runs.end_date,
csu.first_name||' '||csu.last_name Run_By
from
BPM_RUN_PROCESSES runs
inner join BPM_RUN_PROCESS_ENGINES engine on engine.id=runs.process_engine_id
inner join BPM_RUN_STEPS steps on steps.process_instance_id=runs.id
inner join BPM_RUN_THREADS threads on (threads.process_instance_id=runs.id and steps.thread_id=threads.id)
inner join BPM_RUN_OBJECTS obj on (obj.pk_id=runs.id and obj.table_name='BPM_RUN_PROCESSES')
left join BPM_RUN_STEP_COND_RESULTS condres on condres.step_instance_id=steps.id
left join BPM_RUN_STEP_TRANSITIONS trnstn on trnstn.step_instance_id=steps.id
left join BPM_RUN_STEP_ACTION_RESULTS actres on actres.step_instance_id=steps.id
left join BPM_RUN_ASSIGNEES assignee on (assignee.pk_id=actres.id and assignee.table_name='BPM_RUN_STEP_ACTION_RESULTS')
left join BPM_RUN_ASSIGNEE_NOTES notes on notes.run_assignee_id=assignee.id
inner join BPM_DEF_PROCESS_VERSIONS ver on ver.id=runs.process_version_id
inner join BPM_DEF_PROCESSES defn on defn.id=ver.process_id
inner join CMN_CAPTIONS_NLS caption on (caption.table_name='BPM_DEF_PROCESSES' AND caption.language_code ='en' AND caption.pk_id=defn.id)
left join CMN_SEC_USERS csu on csu.id=runs.created_by
where
--defn.process_code='<process code>' and 
runs.id=<process instance id>
group by caption.name, defn.process_code, runs.id, runs.status_code, runs.start_date, runs.end_date, csu.first_name||' '||csu.last_name
order by process_name, process_instance_id