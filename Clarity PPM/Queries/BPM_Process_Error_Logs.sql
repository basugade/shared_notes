select 
caption.name process_name,
defn.process_code,
runs.id process_instance_id,
runs.status_code,
runs.start_date,
runs.end_date,
csu.first_name||' '||csu.last_name Run_By,
be.type_code warning_level,
be.exception_trace2 Log_Message,
arg.arg_value error_argument_value
from 
BPM_ERRORS be 
inner join BPM_RUN_PROCESSES runs on runs.id=be.process_instance_id
inner join BPM_DEF_PROCESS_VERSIONS ver on ver.id=runs.process_version_id
inner join BPM_DEF_PROCESSES defn on defn.id=ver.process_id
inner join CMN_CAPTIONS_NLS caption on (caption.table_name='BPM_DEF_PROCESSES' AND caption.language_code ='en' AND caption.pk_id=defn.id)
left join BPM_ERROR_ARGUMENTS arg on arg.error_id=be.id
left join CMN_SEC_USERS csu on csu.id=runs.created_by
where
defn.process_code = 'kmd_res_term_date_update' AND
UPPER(be.exception_trace2) like UPPER('%resourceDetailsRS%')
and be.type_code = 'BPM_PMT_ERROR'
order by process_instance_id