SELECT 
unique_name EIN,
full_name RESOURCE_NAME,
bti_ja_project_code PROJECT_ID,
bti_task_id TASK_CODE,
to_char(min(pravailstart),'MM-DD-YYYY') ALLOCATION_START_DATE,
to_char(max(pravailfinish),'MM-DD-YYYY') ALLOCATION_END_DATE,
alloc_percentage ALLOCATION_PERCENTAGE,
partition_code ODF_PARTITION_CODE,
primaryroleid PRROLEID,
sum(prallocsum)/COUNT(prallocsum)  AS PRALLOCSUM,
prisrole IS_ROLE, 
'CLARITY CLOUD' PRMODEBY, 
to_char(sysdate,'MM-DD-YYYY') PRMODTIME
FROM 
(
SELECT unique_name,bti_ja_project_code,bti_task_id,pravailstart,pravailfinish,prallocsum,pravailfinish_date,ISFALLBETWEEN,partition_code,prisrole,full_name,alloc_percentage,sum(ISFALLBETWEEN) OVER (PARTITION BY unique_name ORDER BY pravailstart) AS tot_pravailfinish_date,primaryroleid
FROM 
(
SELECT 
unique_name,bti_ja_project_code,bti_task_id,pravailstart,pravailfinish,prallocsum,pravailfinish_date,partition_code,prisrole,full_name,
CASE WHEN pravailfinish_date IS NULL THEN 0 WHEN pravailfinish_date BETWEEN pravailstart AND pravailfinish THEN 0 ELSE 1 end ISFALLBETWEEN,alloc_percentage,primaryroleid
FROM 
(
select srm.unique_name, srm.full_name,odf.bti_ja_project_code,odf.bti_task_id, prtm.pravailstart,prtm.pravailfinish,prtm.prallocsum/3600 prallocsum,odf.partition_code,CASE WHEN pr.prisrole = 0 THEN 'No' WHEN pr.prisrole=1 THEN 'Yes' END prisrole,
LAG (prtm.pravailfinish,1) OVER (PARTITION BY srm.unique_name ORDER BY srm.unique_name, prtm.pravailfinish) AS pravailfinish_date,pr.prprimaryroleid primaryroleid,DECODE(prtm.prallocsum, NULL, 0,DECODE (odfr.bti_con_hrs, 0, 0,(DECODE (odfr.bti_con_hrs, NULL, 0,  (  (prtm.prallocsum / 3600)/ (  (  (  DECODE (prtm.pravailfinish, NULL, inv.schedule_finish,prtm.pravailfinish ) - DECODE (prtm.pravailstart,NULL, inv.schedule_start, prtm.pravailstart ))  + 1  ) / 7) )/ odfr.bti_con_hrs ))))* 100 alloc_percentage
from 
inv_investments inv 
JOIN prteam prtm ON prtm.prprojectid=inv.id
JOIN srm_resources srm ON srm.id=prtm.prresourceid 
JOIN odf_ca_project odf ON odf.id=inv.id
JOIN prj_resources pr ON srm.id = pr.prid
JOIN odf_ca_resource odfr ON odfr.id=srm.id
AND odf.bti_ja_project_code IS NOT NULL AND prtm.LAST_UPDATED_DATE > (SELECT max(csjr.start_date)
FROM cmn_sch_job_definitions csjd 
JOIN CMN_SCH_JOBS csj ON csj.job_definition_id=csjd.id AND csjd.job_code='resource_allocation_extract'
JOIN CMN_SCH_JOB_RUNS csjr ON csjr.job_id=csj.id))))
GROUP BY  unique_name,bti_ja_project_code,bti_task_id,tot_pravailfinish_date,full_name,partition_code,prisrole,alloc_percentage,primaryroleid