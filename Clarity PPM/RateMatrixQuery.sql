SELECT v.matrixkey,
           m.description,
           v.fromdate,
           v.todate,
           DECODE (v.value1, NULL, '*', v.value1)           AS resource_id,
           DECODE (v.value1, NULL, '*', r.full_name)        AS resource_name,
           DECODE (v.value2, NULL, '*', v.value2)           AS input_type_code,
           DECODE (v.value3, NULL, '*', v.value3)           AS resource_class,
           DECODE (v.value4, NULL, '*', ROLE.full_name)     AS Resource_Role,
           DECODE (v.value5, NULL, '*', v.value5)           AS project_id,
           v.numval1                                        Rate,
           v.numval2                                        Standard_Cost,
           v.numval3                                        Actual_Cost,
		   V.last_updated_date
      FROM ppa_matrixvalues  v,
           ppa_matrix        m,
           srm_resources     ROLE,
           srm_resources     r
     WHERE     v.matrixkey = m.matrixkey
           AND v.value2 = ROLE.unique_name(+)
           AND v.value1 = r.unique_name(+)
           AND v.matrixkey = 5000001
           AND SYSDATE BETWEEN V.FROMDATE AND V.TODATE;
